USE myjournal;
-- AUTHOR MOCK DATA
INSERT INTO author (first_name, last_name, email) VALUES ('Dewald', 'Els', 'dewald.els@noroff.no');
INSERT INTO author (first_name, last_name, email) VALUES ('Robert', 'Singer', 'bobby@menofletters.com');
INSERT INTO author (first_name, last_name, email) VALUES ('Dean', 'Winchester', 'dean@menofletters.com');
SELECT * FROM author;

-- JOURNAL MOCK DATA
INSERT INTO journal (name) VALUES ('JavaScript');
INSERT INTO journal (name) VALUES ('Personal');
INSERT INTO journal (name) VALUES ('Work');
INSERT INTO journal (name) VALUES ('Databases');
SELECT * FROM journal;

-- JOURNAL ENTRY MOCK DATA
INSERT INTO journal_entry (title, content, author_id, journal_id) VALUES ('JavaScript Factory', 'Today I learned about JavaScript factory pattern. It was great', 1, 1);
INSERT INTO journal_entry (title, content, author_id, journal_id) VALUES ('Sequelize', 'Learning how to setup sequelize in a Node-Express application', 1, 1);
INSERT INTO journal_entry (title, content, author_id, journal_id) VALUES ('I need sleep', 'I do not sleep enough. I stay up too late and forget the time. I am tired', 1, 2);
INSERT INTO journal_entry (title, content, author_id, journal_id) VALUES ('Vampires', 'Today we discovered shiny vampires. They were very emotional. Weird...', 3, 3);
INSERT INTO journal_entry (title, content, author_id, journal_id) VALUES ('MySQL Joins', 'MySQL Joins are weird. So much to remember. :(', 1, 4);
SELECT * FROM journal_entry;

-- TAG MOCK DATA
INSERT INTO tag (name) VALUES ('javascript');
INSERT INTO tag (name) VALUES ('changes');
INSERT INTO tag (name) VALUES ('discovery');
INSERT INTO tag (name) VALUES ('programming');
INSERT INTO tag (name) VALUES ('sleep');
INSERT INTO tag (name) VALUES ('mysql');
-- SELECT * FROM tag;

-- JOURNAL_TAG MOCK DATA
INSERT INTO journal_entry_tag (journal_entry_id, tag_id) VALUES (1, 1);
INSERT INTO journal_entry_tag (journal_entry_id, tag_id) VALUES (1, 4);
INSERT INTO journal_entry_tag (journal_entry_id, tag_id) VALUES (2, 2);
INSERT INTO journal_entry_tag (journal_entry_id, tag_id) VALUES (2, 5);
INSERT INTO journal_entry_tag (journal_entry_id, tag_id) VALUES (3, 3);
INSERT INTO journal_entry_tag (journal_entry_id, tag_id) VALUES (4, 6);
INSERT INTO journal_entry_tag (journal_entry_id, tag_id) VALUES (4, 4);
INSERT INTO journal_entry_tag (journal_entry_id, tag_id) VALUES (5, 1);
SELECT * FROM journal_entry_tag;

-- SELECT Journal entries with tags and author
SELECT je.journal_entry_id, 
		je.title, 
        je.content, 
        je.created_at, 
        CONCAT(a.first_name, ' ', a.last_name) AS author, 
        a.email AS author_email, 
        GROUP_CONCAT(t.name) AS tags
FROM journal_entry AS je
LEFT JOIN journal_entry_tag AS jt 
	USING(journal_entry_id)
LEFT JOIN tag AS t 
	USING(tag_id)
INNER JOIN author a 
	USING(author_id)
GROUP BY je.title;


