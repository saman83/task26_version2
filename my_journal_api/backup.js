async function connect() {
    try {
        
        await sequelize.authenticate();
        console.log('Connection established');

        const myjournal = await sequelize.query('SELECT * FROM journal', {
            type: QueryTypes.SELECT
        });
        
        const journalNames = myjournal.map(journal => journal.name)

        console.log(journalNames);
        
        //************************************************ */

        const myjournal_entry = await sequelize.query('SELECT * FROM journal_entry', {
            type: QueryTypes.SELECT
        });
        
        const journalTitles = myjournal_entry.map(journal_entry => journal_entry.title)

        console.log(journalTitles);

        // //************************************************ */

        // const myjournal_entry2 = await sequelize.query('INSERT INTO journal_entry (title, content, author_id, journal_id) VALUES (Task26, Today I learned more about MySQL and Express. It was great, 1, 1', {
        //     type: QueryTypes.INSERT
        // });

        // const journalTitles2 = myjournal_entry2.map(journal_entry => journal_entry.title)

        // console.log(journalTitles2);



        // //************************************************ */

        // const languages = await sequelize.query('SELECT * FROM countrylanguage LIMIT 5', {
        //     type: QueryTypes.SELECT
        // });
        
        // const languageNames = languages.map(str => str.Language)

        // // const searchResults3 = languages.filter(languages => languages.countrylanguage.indexOf());

        // console.log(languageNames);


    } catch (e) {
        console.log(e);
        
    }
}

connect();

// A list of journals
// app.get('/v1/api/journals', async (req,res) => {
//     try {
//         console.log('Connection established...');
//         const journals = await sequelize.query('SELECT * FROM journal LIMIT 10', {type: QueryTypes.SELECT});
//         console.log(journals);
//     }
//     catch(e) {
//         console.error(e);
//     }
// return res.status(200).json(journals)
// });