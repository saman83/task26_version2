const express = require("express");
const app = express();
const cors = require("cors");
app.use(cors());
app.use(express.json()); //Tells the application that it's going to use json
const { Sequelize, QueryTypes } = require("sequelize");

//Load our enviroment variables from the .env file
require("dotenv").config();

const sequelize = new Sequelize({
  database: process.env.DB_DATABASE,
  username: process.env.DB_USER,
  password: process.env.DB_PASS,
  host: process.env.DB_HOST,
  dialect: process.env.DB_DIALECT
});

app.get("/", (req, res) => res.send("Hello World!"));

app.get("/v1/api/journals", async (req, res) => {
  const response = { data: null };
  try {
    response.data = await sequelize.query("SELECT * FROM journal", {
      type: QueryTypes.SELECT
    });
  } catch (error) {
    response.data = error;
  }
  return res.json(response);
});

app.get("/v1/api/journal_entry", async (req, res) => {
  const response = { data: null };
  try {
    response.data = await sequelize.query("SELECT * FROM journal_entry", {
      type: QueryTypes.SELECT
    });
  } catch (error) {
    response.data = error;
  }
  return res.json(response);
});

app.post("/v1/api/journal_entry", async (req, res) => {
  const response = { data: null };
  try {
    const title = req.body.title;
    const text = req.body.content;
    const journalId = req.body.journal_id;

    const authorId = req.body.author_id;

    await sequelize.query(
      "INSERT INTO journal_entry (title, content, author_id, journal_id) VALUES (:title, :content, :author_id, :journal_id)",
      {
        replacements: {
          title: title,
          content: content,
          journal_id: journalId,
          author_id: authorId
        },
        type: QueryTypes.INSERT
      }
    );
  } catch (error) {
    response.data = error;
  }
  return res.json(response);
});

app.get("/v1/api/entries", (req, res) => res.send("Hello Entries!"));

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`));

// const title = req.body.title;
// const text = req.body.text;
// const journalId = req.body.journal_id;

// const authorId = req.body.author_id;

// await sequelize.query(
//   "INSERT INTO journal_entry (title, text, author_id, journal_id) VALUES (:title, :text, :author_id, :journal_id)",
//   {
//     replacements: {
//       title: title,
//       text: text,
//       journal_id: journalId,
//       author_id: authorId
//     },
//     type: QueryTypes.INSERT
//   }
// );
