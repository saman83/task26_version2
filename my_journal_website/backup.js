
  //HTML-elements
  const elArticles = document.getElementById('articles');
  const elJournalBtn = document.getElementById('get-journal-entries-btn');


  //EventListeners for buttons
  elJournalBtn.addEventListener('submit', function () {
  renderArticles();
  })


  function renderArticles() {
    elArticles.innerHTML = '';
  
    //Fetch = send network requests to the server and load new information
      fetch('http://localhost:5000/v1/api/journal_entry', data= {
          method: 'POST',
          mode: 'cors',
          headers: {
              'Content-Type': 'application/json'
          },
          body: JSON.stringify({
              title: 'Task26 3d',
              content: 'Blablablabla',
              author_id: '1',
              journal_id: '2'
          })
      })
    .then((response) => {
     //We use now a method to convert the data to json
   return response.json();
    })
     //This is going to be our actual data
    .then((data) => {
      
      data.data.forEach(journal_id => {
        //Let's create div in the for loop so we have to do it for every article
        const elArticle = document.createElement('article') //for backgr. image
        const elArticleTitle = document.createElement('h2')
        const elArticleText = document.createElement('p')
  
        
        elArticleTitle.innerText = journal_id.journal_id
        elArticleText.innerText = journal_id.name
        // elArticle.style.backgroundImage = "url('"+article.urlToImage+"')"      
  
        //AppendChild lägger till det som finns inuti parentesen under elArticle.
        elArticle.appendChild(elArticleTitle)
        elArticle.appendChild(elArticleText);
        elArticles.appendChild(elArticle);
  
      });
      });
    
    
  }